/*! daler-rowney v0.0.1 | (c) 2022 Ash Bryant | MIT License | https://gitlab.com/ashbryant/daler-rowney */
/*
    // NOTE:
    // Custom code goes here and start with $j NOT just $
    // to stop conflicts with themes
*/

var $j = jQuery.noConflict();

$j(document).ready((function() {

    $j('[data-toggle="tooltip"]').tooltip();
    // NOTE:
    // On all pages

    // Responsive helpers
    $j('.main-content iframe, .main-content object, .main-content embed:not(.slider)').not('.keep-sizing').wrap('<div class="embed-container"></div>');


    // Setup isScrolling variable
    // var isScrolling;

    // Listen for scroll events
    // window.addEventListener('scroll', function () {

    //   $j('body').addClass('userisscrolling');

    //   // Clear our timeout throughout the scroll
    //   window.clearTimeout( isScrolling );

    //   // Set a timeout to run after scrolling ends
    //   isScrolling = setTimeout(function() {
    //     $j('body').removeClass('userisscrolling');
    //     // Run the callback
    //     console.log( 'Scrolling has stopped.' );

    //   }, 66);

    // }, false);


    // NOTE:
    // Change the nav on scroll
    //
    let mywindow = $j(window),
        mypos = mywindow.scrollTop(),
        up = false,
        newscroll;

    mywindow.scroll((function() {
        newscroll = mywindow.scrollTop();
        if (newscroll > mypos && mypos > 0 && !up) {
            $j('#wrapper-navbar, body').stop().addClass('scrolling');
            up = !up;
            console.log(up);
            console.log('scrolling down');
            console.log(mypos);
        } else if (newscroll < mypos && up) {
            $j('#wrapper-navbar, body').stop().removeClass('scrolling');
            up = !up;
        }

        mypos = newscroll;
    }));

    // $j('.search.store .btn').on('click', function(){
    //     $j('.mapboxgl-ctrl-geolocate').click();
    // });

    $j('#nav-main button.navbar-toggler.search.d-lg-none').on('click', (function(){
        $j('li.nav-item.dropdown.search.d-none.d-sm-block div.dropdown-menu').toggleClass('show');
    }));

    $j('.navbar-toggler.search').on('click', (function() {
        $j('.mobile-search #inputProductSearch').trigger('focus');
    }));

    $j('.dropdown-menu form.search.store .btn, #banner-stockist form.search.store .btn').click((function() {
        window.location.href = '/store-locator?findme=true';
    }));


    // Passing the location from nav store locator form to the search page (button clicked)
    $j('form.search.current-location .btn').click((function() {
        var currentUrl = window.location.href,
            searhbarpostalcode = $j(this).closest('form').find('.searhbarpostalcode').val().toLowerCase().replace(/ /g, "+"),
            newUrl = '/store-locator?location=' + searhbarpostalcode;

        window.location.href = newUrl;
    }));
    // Passing the location from nav store locator form to the search page (enter pressed)
    $j('form.search.current-location .searhbarpostalcode').keypress((function(e) {
        if (event.keyCode === 13) { //Enter key pressed
            event.preventDefault();
            $j(this).closest('form').find('.btn').click(); // fake it
        }
    }));

    // For the Dymo label effect
    // Allows for multi lines else it would be plain CSS :(
    $j('.bg-dymo, .dymo').each((function() {
        // Don't add it to elements that we have already added it to
        if (!$j(this).hasClass('dymo-added')) {
            $j(this).addClass('dymo-added').wrapInner('<span class="dymo-shadow--wrapper"><span class="dymo-text"></span></span>');
        }
    }));


    $j('.config-inspire a').on('click', (function() {
        // Delete existing values for updated selections
        //Cookies.remove('inputSingle');
        Cookies.remove('inspireUnFamilyactivities');
        Cookies.remove('inspireUnHobbyist');
        Cookies.remove('inspireUnStudent');
        Cookies.remove('inspireUnProfessional');
        // Which materials are you interested in?
        Cookies.remove('inspireUmOil');
        Cookies.remove('inspireUmAcrylics');
        Cookies.remove('inspireUmWatercolourAndGouache');
        Cookies.remove('inspireUmInks');
        Cookies.remove('inspireUmPrinting');
        Cookies.remove('inspireUmDryTechniques');
        Cookies.remove('inspireUmPouring');
        // Which techniques are you interested in?
        Cookies.remove('inspireUtBrushesAndKnives');
        Cookies.remove('inspireUtPrinting');
        Cookies.remove('inspireUtIllustration');
        Cookies.remove('inspireUtPouring');
        Cookies.remove('inspireUtAirbrush');
        // Which surfaces do you like to work on?
        Cookies.remove('inspireUsCanvas');
        Cookies.remove('inspireUsPaper');
        Cookies.remove('inspireUsMural');
        Cookies.remove('inspireUsTextiles');
        Cookies.remove('inspireUsWood');
        Cookies.remove('inspireUsStone');
        // User details
        Cookies.remove('inspireUdEmailMe');
        Cookies.remove('inspireUdFirstName');
        Cookies.remove('inspireUdLastName');
        Cookies.remove('inspireUdEmailAddress');
    }));

    if ($j('.results-list')) {
        $j('ul.view-toggle li').on('click', (function() {

            $j('ul.view-toggle li').toggleClass('active');

            if ($j('ul.view-toggle li.grid.active')) {
                $j('#results').toggleClass('view--grid');
            }

        }));
    }

    // Language/country select menu
    $j('.btn.select-country').on('click', (function() {
        event.preventDefault();
        $j('.select-country-wrapper').css('display', 'grid'); // Show
    }));

    $j('.select-country--top a').on('click', (function() {
        event.preventDefault();
        $j('.select-country-wrapper').css('display', 'none'); // Hide (default)
    }));

    // Have we cookies set already?
    let country = Cookies.get('country'),
        lang = Cookies.get('lang');

    // If we have saved cookies for country & lang selection
    if (country && lang) {
        $j('a.select-country span.flag-icon').removeClass().addClass('flag-icon flag-icon-' + country);
    }

    // Set cookies for country & language selection
    $j('.select-country--content a.country-lang-link').on('click', (function(event) {
        event.preventDefault();
        let selectedCountry = $j(this).data('country'),
            selectedLang = $j(this).data('lang');

        // console.log('Country = ' + selectedCountry + '. Lang = ' + selectedLang);

        Cookies.set('country', selectedCountry);
        Cookies.set('lang', selectedLang);
        $j('a.select-country span.flag-icon').removeClass().addClass('flag-icon flag-icon-' + selectedCountry);
    }));

    $j('.carousel--full-width.quote').each((function() {
        $j(this).find('.carousel-item:first').addClass('active');
    }));


    // NOTE:
    // For desktops
    $j('.menu span.flyout-menu > a.has-submenu').on('click', (function() {
        event.preventDefault();
        if ($j(this).hasClass('active')) {
            $j(this).removeClass('active');
        } else {
            $j(this).addClass('active');
            $j(this).parent().siblings().find('a.has-submenu').removeClass('active');
        }
    }));

    $j('a.close-dropdown').on('click', (function() {
        event.preventDefault();

        $j('.dropdown-menu').removeClass('show');
        $j('a.has-submenu').removeClass('active');
    }));

    // // NOTE:
    // // For mobiles
    // if ($j(window).width() < 769) {
    //     $j('.menu > span a').on('click', function() {
    //         event.preventDefault();

    //         if ( $j(this).hasClass('active')) {
    //             $j(this).removeClass('active');
    //         } else {
    //             $j(this).addClass('active');
    //             $j(this).siblings().removeClass('active');
    //         }
    //     });
    // }

    var menuHeight = $j('.menu > a + .submenu').height();

    $j('.dropdown-menu').click((function(e) {
        e.stopPropagation();
    }));

    var windowsize = $j(window).width();
    $j(window).resize((function() {
        windowsize = $j(window).width();
    }));

    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (windowsize < 992) {

            if (prevScrollpos > currentScrollPos) {
                $j("#main-scroll-menu").css('top', 0);
                $j("#scroll-menu").css('top', -$j("#scroll-menu").outerHeight());
            } else {
                $j("#main-scroll-menu").css('top', -$j("#main-scroll-menu").outerHeight());
                $j("#scroll-menu").css('top', 0);
            }

        } else {
            if (prevScrollpos > currentScrollPos) {
                $j("#flow-menu").css('top', '-54px');
            } else {
                $j("#flow-menu").css('top', '43px');
            }
        }
        prevScrollpos = currentScrollPos;
    }

    $j("#scrollToTop").click((function() {
        $j("html, body").animate({
            scrollTop: 0
        });
        return false;
    }));
}));